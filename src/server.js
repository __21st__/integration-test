const request = require('express');
const express = require('express');
const converter = require('./converter');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send('Hello World!')
});

// endpoint http://localhost:3000/rgb-to-hex?red=255&green=128&blue=0
app.get('/rgb-to-hex', (req, res) => {
    const red = parseInt(req.query.red, 10);
    const green = parseInt(req.query.green, 10);
    const blue = parseInt(req.query.blue, 10);
    // FIXME
    res.send(converter.rgbToHex(red, green, blue));
});

var jsonParser = bodyParser.json();

// endpoint http://localhost:3000/hex-to-rgb?hex=ffffff
app.get('/hex-to-rgb', (req, res) => {
  const hex = req.query.hex;
  const rgb = converter.hexToRgb(hex)
  res.send(rgb);
});


if (process.env.NODE_ENV === 'test') {
    module.exports = app;
} else {
    // app.listen(port, () => console.log(`Server: localhost: ${port}`));
}

module.exports = app;