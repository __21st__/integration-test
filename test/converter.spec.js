const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("RGB to Hex conversion", () => {
        it("converts basic colors", () => {
            const redHex = converter.rgbToHex(255, 0, 0); // ff0000
            const greenHex = converter.rgbToHex(0, 255, 0); // 00ff00
            const blueHex = converter.rgbToHex(0, 0, 255); // 0000ff

            expect(redHex).to.equal("ff0000");
            expect(greenHex).to.equal("00ff00");
            expect(blueHex).to.equal("0000ff");
        });
    });
    describe("Hex to RGB conversion", () => {
        it("converts basic colors again", () => {
            const redRGB = converter.hexToRgb("ff0000");
            const greenRGB = converter.hexToRgb("00ff00");
            const blueRGB = converter.hexToRgb("0000ff");

            expect(redRGB).to.deep.equal({ red: 255, green: 0, blue: 0 });
            expect(greenRGB).to.deep.equal({ red: 0, green: 255, blue: 0 });
            expect(blueRGB).to.deep.equal({ red: 0, green: 0, blue: 255 });

        })
    })
});